/*
 * tableModel.java
 *
 * Created on 26 de Mar�o de 2007, 11:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Modelo;
import javax.swing.table.AbstractTableModel;
import java.sql.*;

/**
 *
 * @author Hugo
 */
public class ModelodeTabela extends AbstractTableModel{
    private int colunas;
    private int linhas;
    private ResultSet ResultSets;
    private ResultSetMetaData metaData;
    /** Creates a new instance of tableModel */
    public ModelodeTabela(ResultSet rs) throws SQLException {
        ResultSets = rs;
        metaData = rs.getMetaData();
        
        //determina o n�mero de linhas em um ResultSet
        rs.last(); //move o cursor para o ultimo registro
        linhas = rs.getRow(); // pega o numero de linhas
        
        //notifica JTable das altera��es
        fireTableStructureChanged();
    }

    public int getRowCount() {
        return linhas;
    }

    public int getColumnCount() {
        try {
            //determina o numero de colunas
            return metaData.getColumnCount();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        // se houver problema retorna 0
        return 0;
        
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            ResultSets.absolute(rowIndex + 1);
            return ResultSets.getObject(columnIndex + 1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        // caso de algum problema retorna vazio
        return "";
                
    }
    
    public String getColumnName (int column){
        //determina o nome da coluna
        try{
            return metaData.getColumnName(column + 1);
        }catch(SQLException ex) {
            ex.printStackTrace();
            
        }
        // caso de algum problema retorna vazio
        return "";
    }
}
