/*
 * ConnectDB.java
 *
 * Created on 05 de Abril de 2008, 10:50
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Modelo;
import java.sql.*;
import java.util.regex.*;

/**
 *
 * @author Alexsandro Monteiro Carneiro
 */
public class ConnectDB {
    // Objeto de COnex�o com BD
    private Connection conn;
    // Objeto de Consulta SQL
    static public Statement stmt;
    // Objeto com dados SQL
    private ResultSet res;
    static public ResultSet resultado=null;
    //contador de tuplas
    private int affectRows;
    /** Creates a new instance of ConnectDB */
    public ConnectDB() {
    }
    //Fun��o de Conex�o com PostgreSQL via JDBC
    public void init(){
       try
       {
        Class.forName("org.postgresql.Driver");
        conn = DriverManager.getConnection(
               "jdbc:postgresql://localhost:5432/SupermercadoUCDB","postgres", "postgres");
        System.out.println("Conectado ao PostGreSQL.");        
        }catch(Exception e){
            System.out.println("Falha ao tentar a conex�o");
            e.printStackTrace();
        }
       
       try{
           stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);      
       }catch(Exception e){
           System.out.println("Falha no Cursor de Execu��o");
           e.printStackTrace();
       }
       
    }
    
      static public void SQL(String comando) throws Exception
    {
        try
        {
            resultado=stmt.executeQuery(comando);
        }//Fim do try
        catch(Exception e)
        {
            e.printStackTrace();
            //throw new Exception("Erro na execu��o do comando SQL!");
        }//Fim do catch(Exception e1)        
    }
    
    public Connection getConnection(){
        return conn;
    }
    
    public Statement getStatement(){
        return stmt;
    }
    
    public void close(ResultSet rs){
        if(rs != null){
            try{
               rs.close();
            } catch(Exception e){}
        }
    }
    
    public void close(Statement s){
        if(s !=null){
            try{
               s.close();
            }catch(Exception e){}
        }
    }
    
  public void destroy(){
    if(conn !=null){
         try{
               conn.close();
         } catch(Exception e){}
    }
  } 
  
}