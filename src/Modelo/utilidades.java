/*
 * utils.java
 *
 * Created on 21 de Fevereiro de 2007, 18:48
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Modelo;
import java.util.regex.*;
/**
 *
 * @author Hugo
 */
public class utilidades {
    
    /** Creates a new instance of utils */
    public utilidades() {      
        
    }
    
    /**
     * @param panel
     */
     public void limpaCampos(javax.swing.JPanel panel){
         java.awt.Component[] componentes = panel.getComponents();
         
         javax.swing.JTextField textfield = null;
         javax.swing.JComboBox combo = null;
         
         for(int i = 0; i < componentes.length; i++){
             if (componentes[i] instanceof javax.swing.JTextField){
                 textfield = (javax.swing.JTextField) componentes[i];
                 textfield.setText("");
             }
            
             if (componentes[i] instanceof javax.swing.JComboBox){
                 combo = (javax.swing.JComboBox) componentes[i];
                 combo.setSelectedIndex(0);
             }
             
//             if (componentes[i] instanceof javax.swing.ButtonGroup){
//                componentes[i].clearSelection();
//             }
         }
     }   
     
     public void centerForm(javax.swing.JFrame frm, javax.swing.JFrame frmMain){
         java.awt.Dimension tFrame = frmMain.getSize();
         java.awt.Dimension tFrm   = frm.getSize();
         java.awt.Point loc = frmMain.getLocation();
         
         frm.setLocation(
                 (tFrame.width - tFrm.width)/2 + loc.x,
                 (tFrame.height - tFrm.height)/2 + loc.y);
         
     }
     
     public void showMessage(javax.swing.JFrame frm, String msg){
         javax.swing.JOptionPane w = new javax.swing.JOptionPane();
         w.showMessageDialog(frm, msg);
     }
     
     public boolean validarCampo(String campo, char tipo){
         Pattern padrao = null;
         Matcher pesquisa = null;
         int i = 0;
         switch (tipo){
             case 'C':
                 if (campo.length() != 10) 
                    return false;
                 for (i=0;i<campo.length(); i++){
                    // CEP 99.999-999
                    if (i == 0 || i == 1 || i == 3 || 
                        i == 4 || i == 5 || i == 7 || 
                        i == 8 || i == 9){
                        if (!(campo.charAt(i) == '0' || campo.charAt(i) == '1' || campo.charAt(i) == '2' ||
                              campo.charAt(i) == '3' || campo.charAt(i) == '4' || campo.charAt(i) == '5' ||
                              campo.charAt(i) == '6' || campo.charAt(i) == '7' || campo.charAt(i) == '8' ||
                              campo.charAt(i) == '9'))
                            return false;
                            
                    }
                    if (i == 2 && campo.charAt(i) != '.')
                        return false;
                    if (i == 6 && campo.charAt(i) != '-')
                        return false;
                }
                
                break;
             
             case 'T':
                 padrao = Pattern.compile("\\(\\d{2}\\)\\d{4}-\\d{4}");   
                 pesquisa = padrao.matcher(campo);  
                 if (!pesquisa.matches())  return false;
                 break; 
             case 'E':
                 //import java.util.regex.*;
                 padrao = Pattern.compile(".+@.+\\.[a-z]+");   
                 pesquisa = padrao.matcher(campo);  
                 if (!pesquisa.matches())  return false;
                 break; 
         }
         return true;
     }
}
